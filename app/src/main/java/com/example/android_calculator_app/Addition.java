package com.example.android_calculator_app;

public class Addition implements GetResult{
    private int num1;
    private int num2;
    @Override
    public int getResult(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
        return num1 + num2;
    }
}
