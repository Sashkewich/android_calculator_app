package com.example.android_calculator_app;

public class Subtraction implements GetResult{
    @Override
    public int getResult(int num1, int num2) {
        int result = num1 - num2;
        return result;
    }
}
