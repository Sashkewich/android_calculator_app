package com.example.android_calculator_app;

import android.annotation.SuppressLint;

public class Calculator {
    private int firstNum; // первое число
    private int secondNum; // второе число
    private int actionSelected; // какой знак операции сейчас в работе

    private Addition addition; // Сложение

    private Subtraction subtraction; // Вычитание
    private Multiplication multiplication; // Умножение
    private Division division; // Деление

    private StringBuilder inputStr = new StringBuilder(); // через него будем работать со строкой
    private State state; // здесь будет хранится состояние

    private enum State { // перечисление состояний: ввод первого числа, ввод второго числа, показ результата
        firstNumInput, secondNumInput, showResult
    }

    // инициализация калькулятора, состояние которого начинается с ввода первого числа
    public Calculator() {
        state = State.firstNumInput;
    }

@SuppressLint("NonConstantResourceId")
    public void onNumPressed(int buttonId) {
        if (state == State.showResult) { // если нынешнее состояние калькулятора - показ результата
            state = State.firstNumInput; // текущее состояние калькулятора - ввод первого числа
            inputStr.setLength(0); // строка с числом очищается
        }

        if (inputStr.length() < 9) { // если число в строке состоит меньше чем из 9 цифр
            switch (buttonId) { // проверяем нажатую кнопку с числом
                case R.id.button0: // если нажата кнопка с цифрой 0
                    if (inputStr.length() != 0) { // если строка не пуста
                        inputStr.append("0"); // добавляем в строку 0
                    }
                    break;

                case R.id.button1: // если нажата кнопка с цифрой 1
                    inputStr.append("1"); // добавляем в строку 1
                    break;

                case R.id.button2: // и так далее
                    inputStr.append("2");
                    break;

                case R.id.button3:
                    inputStr.append("3");
                    break;

                case R.id.button4:
                    inputStr.append("4");
                    break;

                case R.id.button5:
                    inputStr.append("5");
                    break;

                case R.id.button6:
                    inputStr.append("6");
                    break;

                case R.id.button7:
                    inputStr.append("7");
                    break;

                case R.id.button8:
                    inputStr.append("8");
                    break;

                case R.id.button9:
                    inputStr.append("9");
                    break;
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void onActionPressed(int actionId) {
        if (actionId == R.id.buttonEquals && state == State.secondNumInput
                && inputStr.length() > 0) { // если нажали на кнопку "=" и если состояние калькулятора - ввод второго числа
            secondNum = Integer.parseInt(inputStr.toString()); // получаем второе число, распарсивая полученную строку
            state = State.showResult; // а состояние калькулятора переходит в "Показ результата"
            inputStr.setLength(0); // строка с числом очищается

            switch (actionSelected) { // в зависимости, какой какая кнопка под знаком операции нажата,
                case R.id.buttonPlus: // если нажата кнопка "+",
                    this.addition = new Addition();
                    inputStr.append(addition.getResult(firstNum, secondNum)); // в строку добавляем полученный результат сложения
                    break;

                case R.id.buttonMinus: // если нажата кнопка "-",
                    this.subtraction = new Subtraction();
                    inputStr.append(subtraction.getResult(firstNum, secondNum)); // в строку добавляем полученный результат вычитания
                    break;

                case R.id.buttonMultiply: // если нажата кнопка "*",
                    this.multiplication = new Multiplication();
                    inputStr.append(multiplication.getResult(firstNum, secondNum)); // в строку добавляем полученный результат умножения
                    break;

                case R.id.buttonDivide: // если нажата кнопка "/",
                    this.division = new Division();
                    inputStr.append(division.getResult(firstNum, secondNum)); // в строку добавляем полученный результат деления
                    break;
            }
        }

        else if (inputStr.length() > 0 && state == State.firstNumInput) { // если строка с числом пуста и если состояние калькулятора - ввод первого числа
            firstNum = Integer.parseInt(inputStr.toString()); // получаем первое число, распарсивая полученную строку
            state = State.secondNumInput; // а состояние калькулятора переходит в "Ввод второго числа"
            inputStr.setLength(0); // строка с числом очищается

            switch (actionId) { // в зависимости, какой знак операции нажат
                case R.id.buttonPlus: // если нажата кнопка "+",
                    actionSelected = R.id.buttonPlus; // калькулятор будет знать, что нужно произвести операцию сложения
                    break;

                case R.id.buttonMinus: // если нажата кнопка "-",
                    actionSelected = R.id.buttonMinus; // калькулятор будет знать, что нужно произвести операцию вычитания
                    break;

                case R.id.buttonMultiply: // если нажата кнопка "*",
                    actionSelected = R.id.buttonMultiply; // калькулятор будет знать, что нужно произвести операцию умножения
                    break;

                case R.id.buttonDivide:
                    actionSelected = R.id.buttonDivide; // калькулятор будет знать, что нужно произвести операцию деления
                    break;
            }
        }
    }

    public String getText() {
        return inputStr.toString(); // возвращает полученную строку
    }

    public void reset(){ // обнуление
        state = State.firstNumInput; // состояние калькулятора переходит в "Ввод первого числа"
        inputStr.setLength(0); // строка с числом очищается
    }
}
