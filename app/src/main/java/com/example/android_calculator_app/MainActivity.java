package com.example.android_calculator_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private Calculator calculator;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        // В этом списке находятся все ID кнопок-чисел (от 0 до 9)
        ArrayList<Integer> numbersIds = new ArrayList<>(Arrays.asList(
                R.id.button0,
                R.id.button1,
                R.id.button2,
                R.id.button3,
                R.id.button4,
                R.id.button5,
                R.id.button6,
                R.id.button7,
                R.id.button8,
                R.id.button9
        ));

        // В этом списке находятся все ID кнопок-знаков операций (+, -, *, /, =)
        ArrayList<Integer> actionsIds = new ArrayList<>(Arrays.asList(
                R.id.buttonPlus,
                R.id.buttonMinus,
                R.id.buttonMultiply,
                R.id.buttonDivide,
                R.id.buttonEquals
                ));

        text = findViewById(R.id.previousCalculationView);
        calculator = new Calculator();


        View.OnClickListener numberButtonClickListener = view -> {
            calculator.onNumPressed(view.getId());
            text.setText(calculator.getText());
        };

        View.OnClickListener actionButtonOnclickListener = view -> {
            calculator.onActionPressed(view.getId());
            text.setText(calculator.getText());
        };

        for (int i = 0; i < numbersIds.size(); i++) {
            findViewById(numbersIds.get(i)).setOnClickListener(numberButtonClickListener);
        }

        for (int i = 0; i < actionsIds.size(); i++) {
            findViewById(actionsIds.get(i)).setOnClickListener(actionButtonOnclickListener);
        }

        findViewById(R.id.buttonCancel).setOnClickListener(view -> {
            calculator.reset();
            text.setText(calculator.getText());
        });
    }

}