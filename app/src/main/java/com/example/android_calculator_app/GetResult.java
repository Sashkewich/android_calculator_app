package com.example.android_calculator_app;

public interface GetResult {
    int getResult(int num1, int num2);
}
